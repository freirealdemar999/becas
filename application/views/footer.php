<footer class="footer bg-dark text-white">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h5>Informacion</h5>
                <ul class="list-unstyled">
                    <li><a href="#">Elaborado poro:Aldemar Freire</a></li>
                    <li><a href="#">Ingenieria en sistemas</a></li>
                    <li><a href="#">7 "A"</a></li>
                </ul>
            </div>
            <div class="col-md-6">
                <h5>Contacto</h5>
                <ul class="list-unstyled">
                    <li><i class="fas fa-map-marker-alt"></i> Dirección: LAtacunga UTC</li>
                    <li><i class="fas fa-phone"></i> Teléfono:87979797</li>
                    <li><i class="fas fa-envelope"></i> Correo electrónico: aldemar.freire3991@utc.edu.ec</li>
                </ul>
            </div>
        </div>
        <hr>
        <div class="text-center">
            <span class="text-muted">© 2023 BECAS ESTUDIANTES. Todos los derechos reservados.</span>
        </div>
    </div>
</footer>

<!-- Aquí finaliza el contenido de tu página -->
</body>
</html>