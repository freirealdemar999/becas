<!DOCTYPE html>
<html>
<head>
    <title>Tipos de Becas</title>
</head>
<body>
    <div class="container">
        <h1>Tipos de Becas</h1>
        <table class="table">
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Descripción</th>
                    <th>Requisitos</th>
                </tr>
            </thead>
            <tbody>
            <td>Monetaria</td>
            <td>dinero por buenas calificaciones</td>
            <td>Tener mas de 8.5</td>
            </tbody>
            <tbody>
            <td>Monetaria</td>
            <td>dinero por bajos recursos</td>
            <td>Evidencias de bajos recusos</td>
            </tbody>
            <tbody>
            <td>Discapacidad</td>
            <td>dinero por alguna discapacidad</td>
            <td>carnet mayor al 50%</td>
            </tbody>
        </table>
    </div>
</body>
</html>
