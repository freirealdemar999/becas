<h1>NUEVO ASPIRANTE A BECA</h1>
<form class="" action="<?php echo site_url(); ?>/instructores/guardar" method="post">
  <div class="row">
    <div class="col-md-4">
      <label for="">CÉDULA:</label>
      <br>
      <input type="number"
      placeholder="Ingrese la cédula"
      class="form-control" name="cedula_ins" value="">
    </div>
    <div class="col-md-4">
      <label for="">Primer Apellido:</label>
      <br>
      <input type="text"
      placeholder="Ingrese el primer apellido"
      class="form-control" name="primer_apellido_ins" value="">
    </div>
    <div class="col-md-4">
      <label for="">Segundo Apellido:</label>
      <br>
      <input type="text"
      placeholder="Ingrese el segundo apellido"
      class="form-control" name="segundo_apellido_ins" value="">
    </div>
    <div class="col-md-4">
      <label for="">Nombres:</label>
      <br>
      <input type="text"
      placeholder="Ingrese sus nombres"
      class="form-control" name="nombres_ins" value="">
    </div>
    <div class="col-md-4">
      <label for="">Título:</label>
      <br>
      <input type="text"
      placeholder="Ingrese su título"
      class="form-control" name="titulo_ins" value="">
    </div>
    <div class="col-md-4">
      <label for="">Teléfono:</label>
      <br>
      <input type="text"
      placeholder="Ingrese el teléfono"
      class="form-control" name="telefono_ins" value="">
    </div>
    <div class="col-md-12">
      <label for="">Dirección:</label>
      <br>
      <input type="text"
      placeholder="Ingrese la dirección"
      class="form-control" name="direccion_ins" value="">
    </div>
  </div>
  <br>
  <div class="col-md-12 text-center">
    <button type="submit" name="button" class="btn btn-primary">
      GUARDAR
    </button>
    &nbsp;
    <a href="<?php echo site_url(); ?>/instructores/index" class="btn btn-danger">CANCELAR</a>
  </div>
</form>
